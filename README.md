# README #

This a collection of my notes on working with Linux on the command line, in MarkDown format so the repo can be used as a tiny wiki to quickly find what I need.

Keeping them here means never losing them again, or having to look in another pc's shell history how I solved something a few weeks/months ago. As it stands they're pretty basic, and may include uni notes as well. 

You're free to suggest errors/improvements. If you use them, you do so at your own responsibility.