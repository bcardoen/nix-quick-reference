Networking q-ref. 
=================

### Disclaimer
This document is a collection of the most frequently used/needed networking related commands when working with a *Nix system. Sources are manpages, Google, Red Hat networking documentation and many others. Even if there are no errors in the commands themselves, they provide endless ways to cripple your system, for which I take no responsibility whatsoever.  
Unless otherwise assumed, the system is a Fedora derivative. Only firewalld is fedora specific, as are the locations of the network scripts. The rest is applicable in for example dd-wrt.   
### Legend : 
*       \# root shell cmd
*       $ user shell cmd (possibly requiring wheel/wireshark group access)
*       A command followed by another indicates an equivalent/deprecating set.
*       // inline comment describing command
*       ! here be dragons  

A final warning : Nearly all these commands change the system state (at your own risk), but are also non-persistent. Persistent network configuration depends on you setup, but is handled by {NetworkManager, network service and or a set of interface scripts}.
For example in RHEL/Fedora look at  
    /etc/sysconfig/network-scripts  
for persistent configuration of any iface. 


### 0. Netstat  
    #netstat -tulpna

Show all [l]istening connections using [u]DP, [t]CP, using [n]umeric addressing and resolve [p]rogram ids resposible for the connections. The answer to "Is cups listening on port 631 or 632?".  

    $netstat -i  
    
Print statistics (for all ifaces). Usefull to quickly determine what interface is doing anything at all (and doing it correctly wrt errs)  

    $netstat -rn
    $ip route 
Show kernel routing table. Flags [U]sed, [G]ateway.

    $netstat -s
A hideous amount of statistics about all aspects of the networking stack, for a rainy day.

### 1. IP Tools
New set of tools that replace/deprecate ifconfig, netstat, route and many more.

    $ip addr show
    $ifconfig [-a]
Shows interfaces with state, L2/L3 address configuration. One of the first commands you enter when trying to see what's what on a new system. Note that ifconfig doesn't show 'down' devices witout -a.

    #ip addr add <ip>/mask dev <iface>    
    #ip addr del <ip>/mask dev <iface>
Manually set/remove address to iface. (non persistent)

    #ip addr flush dev <iface> 
Zero interface. (update routing table or face 'interesting' consequences)

    #ip link set <device> [up|down]
    #ifconfig <device> [up|down]
Bring a device up/down. Requires that the corresponding kernel module is loaded (else iface isn't even registered). 

    #ip link add <name> type ifacetype
Add interface to system. Typically usage for dummy devices:
    #ip link add mydummy type dummy  
This relies on the (insmodded) dummy kernel module. 

    $ip route show // see netstat
    $ip ro
    $route -n
Summarized routing table

    #ip route add <target> via <node> dev <device>
    #ip route del <target>
Note: if you get a cryptic "network unreachable" error here, you picked a node which isn't reachable on the same network as the current system. Common sense dictates that such a node makes a poor target for a routing table.

    #ip route flush dev <iface>   
Remove routing tables matching iface. (typically after/before you flush the iface itself).

Confused about netmask, private addresses and subnet sizing ?  

    $man ipcalc

### 2. Sysctl.
Sysctl isn't network specific, but quite a few of the tunables it controls are vital for the network stack and you will need to at least query, if not change, them from time to time.  
The proc pseudo filesystem gives you access to these

    $echo /proc/sys/...  
    #cat param > /proc/sys/...
    $sysctl p.q.z.r.param

Where p.q.z.r corresponds with /proc/sys/p/q/z/r, ie the 'namespace' of the parameter.

    #sysctl -w param=value
Set param to value. Applies immediately, but is *not persistent*.

    #vi /etc/sysctl.conf or vi /etc/sysctl.d/<prior>=sysctl.conf
    #sysctl -p modified_sysctl.conf
Replace <prior> with an integer, lower is higher priority (executed in ascending order). Or laugh/cry when your package manager decides to overwrite sysctl.conf.

### 3. ARP
Quick refresher : [A]ddress [R]esolution [P]rotocol, handles MAC<>IP translation.

    $arp -a
    $ip neigh
    
Query arp cache. As you can see from ip neigh's output, an entry in the arp cache has more state than either in or out of the cache. 

    #arp -s <IP> <MAC>
    
Set a static entry in the cache. 

    #ip neighbor add/del <ipv4/6> lladdr <MAC> dev <iface> nud permanent
    #arp -d host
    
Remove (usually for static entries).


### 4 Bridging
Bridging (in the classical interpretation) links 2 (or more) L2 segments of a network. A (transparant) bridge works on L2, but can be 'enhanced' with L3. A bridge controls n interfaces, in Linux this can be done in software (and needed for instance if/when giving VM's access to your physical net. Note that in Linux, a bridge is an interface as well.

    #brctl show
General listing of all bridges.

    #brctl showmacs <bridge>
Get the forwarding tables of <bridge>, or rather the table of learned MAC addresses per interface. 

    #brctl addbr <bridge> 
    #brctl addif <bridge> <iface>
    
Delif for the reverse. Add iface as an interface to the bridge. Any traffic into iface is considered as incoming on bridge, and iface is considered as an output interface. Note that bridge typically is an interface itself.

    #brctl stp <bridge> [on|off]
Enable/Disable spanning tree algorithm on bridge. Without stp a single loop in your L2 toplogy will have dire consequences.

    #brctl showstp <bridge>
    
Show the current state of the bridge and its ports (ifaces) in the stp algorithm.

### 5. Firewalld
Why use it instead of iptables ? The main reason is v4/v6 handling within 1 configuration set. Under the hood firewalld uses IPTables. Some things such as simple logging (i.c.t. complex rich rule logging) are as of writing not implemented.  
Usually you test a new rule in runtime configuration, if all goes well apply it to permanent and then reload permanent (persistent). 

    #firewall-cmd [--permanent] [--zone=myzone or default] <act>

As stated, if you leave permanent out you're changing the runtime config, and without zone specified the default zone is used.

Querying zones:

    #firewall-cmd --get-default-zone | --set-default-zone=myzone
    #firewall-cmd --get-active-zones

    #firewall-cmd --reload
Reload makes the permanent configuration (which you likely just edited) the runtime configuration, but WITHOUT losing state (ie all connection tracking state is preserved)

    #firewall-cmd --complete-reload
Same as above, but drop all connection tracking state.

    #firewall-cmd --runtime-to-permanent
Save the changes to the runtime configuration as permanent. So instead of adding rules to permanent (after testing in runtime), you immediately save them to permanent.

    #firewall-cmd --permanent --zone=<zone> --add-interface<iface>
You have a new or unassigned interface, and want it assigned to a zone. Example : you want unrestricted access to the loopback interface, use this with zone 'trusted'.

    #firewall-cmd --permanent --[add|del]-port=xyz/proto
Open/close port xyz for protocol listed. Note that xyz can be a range.   
Example: you have a server with a nuttcp server instance running, that listens on tcp 5000 to control data and a client that wants to send test data in 3 parallel streams (tcp 5001-5004) :

    #firewall-cmd --add-port=5000-5004/tcp

### 6. SSH

    $ssh [-6] [-p 22] user@host [-t "cmd"]
Login to host as user, optionally execute "cmd". p specifies the ssh server port, which can vary from the default 22. Use 6 to force ipv6. 

    $ssh -vvv <args>
Essential to find out what exactly is going wrong (cipher, identity, configuration, network,...)

The Dropbear ssh server is not always compatible with openssh, or it may be that ssh isn't installed at all:

    $dbclient -i [identity] user@host  

And if you don't want to retype your passphrase a billion times (e.g. git)


    $ssh-add [keyfile]  

Without parameter, it adds to its chain the pair in ~/.ssh/, after you entered your passphrase. While this has advantages, you now will have to make sure that your laptop/station is locked if you're not at the console.

#### 6.0 Tricks
One of the most often occurring mistakes when working in ssh'ed sessions is typing commands intended for another machine in the wrong shell. Visually altering the console/prompt can help avoid this :

    $echo env | grep SSH
Ssh defines on successful connection several environment variables, which you can use to differentiate your session. A trivial example of using these
in ~/.bashrc:

    if [ -n "$SSH_CLIENT" ];
    	then PS1="__SSH__"$PS1;
    fi;    

and lest you forgot, you don't need a relogin for that to take effect:

    source ~/.bashrc
#### 6.1 TCPDump over SSH

Using wireshark over ssh: this can be done in a variety of ways, one of the most simple is :  
    
    $ssh root@192.168.1.12 tcpdump -w - 'port !22' | wireshark -k -i -  

Forgetting for a moment that rootlogin via ssh is not that smart, this command establishes a ssh connection to the remote host, executes tcpdump on it, instructs tcpdump to ignore any ssh traffic (which would be anything in the tunnel itself) and pipes it to wireshark. The - on the end instructs wireshark to read from stdin, the k starts capture immediately and -i denotes the interface, in this case stdin. The -w flag on tcpdump writes to wiresharks input.

#### 6.2 SSHFS
But sometimes you need more than single file access, which is where sshfs comes in :   
 
    $mkdir mymountpoint  

    $sshfs user@host:/remotedirectory ./mymountpoint  

After the pwd prompt, verify with 
 
    $mount | grep ssh  

That the remote directory is mounted and you can now use that filesystem over the net. (take rtt into account!) 

    $sudo umount mymountpoint  

Neatly dismounts the filesystem. Anything you didn't copy to your actual filesystem is, obviously, no longer accessible. (note that this uses fuse on my system, ymmv).
    
### 7. Tcpdump

    #tcpdump -n -l > outfile & tail -f outfile
Write intercepted traffic to outfile, using [l]inebuffering and don't resolve [n]ames, print outfile to console as it is appended.

    #tcpdump host <trg>
Only select traffic from/to host

    #tcpdump -i virbr0 -nnXSl -s0 src 192.168.122.1 or dst 192.168.122.1 and icmp
On the specified [i]nterface, capture the entire packet (-s0) from or to ip if protocol is icmp
Args:
* -c<cnt> : only capture cnt
* -w file to write dump to
* -r file to read old dump from
* -XS: hexdump packet, and display ascii next to it.
* -n : no hostname lookup
* -nn : and no service lookup

Filters:
* Address : <ip>/mask
* Protocols : tcp/udp/ip/icmp
* Ports : src port xyz dst port xwd
* Operators : and &&, or ||, not !
* Grouping : () !!!! Escape in shell or single quote specifier string

Packet inspection: 

    #tcpdump 'tcp[13] & 2 !=0'
Look at 13th octet in tcp header (flags), bitmask with 2 and test if set.

    #tcpdump 'tcp[tcpflags] and tcp-syn != 0'
The slightly more humane equivalent.

    #tcpdump -i enp2s0 -nnXSl -s0 host 192.168.1.1 and tcp and port 80 and less 400
Intercept traffic from/to ip, capture entire packet (s0), only look at tcp port 80 and packets of length less than 400 bytes. 

### 8. Wireshark
These are very basic notes regarding wireshark usage.

Capture filter : kernel module, specify (with basic syntax) which packets are icepted, syntax shared with tcpdump. Capture filters are set when starting a capture.

Display filter : view on capture filter. More powerful with a differing syntax, but more overhead (userspace).

operators :
* eq/ne/lt/ge/le/gt

* and or xor not [] (substr)


    [n:m] := begin at n, count m (so n+m is excluded)

    eth.src[1-2]= fields 1,2 of mac address (range== x 1 2 x x )
    
    [:4] : first 4 (minus 4 itself, so 0123
    [4:] :

    Combining : [0:2,1-2] == 0,1,1,2
    
Membership operator . (where member is protocol dependent)

    tcp.port==<x>   // Match tcp port x
    tcp.flags.syn   // if(exists)
    ip.addr==<ipv4> // or ipv6
    
DON'T

    ip.addr!=xyz // if $src neq xyz, evaluates to true (not what you wanted)
DO

    !(ip.addr==xyz)
Example:
nuttcp uses port 5000 for control server, when looking at data we want to skip that traffic

    tcp and !(tcp.dstport == 5000 or tcp.srcport == 5000)

Graphing:
* Statistics > Flow Graph for conversation view
* Statistic > Time Sequence graph (tcptrace)

     Window size (allow seqnrns is upper grey line
    
     lower line is acked seq nrs
    
     in between (window size) is tcp stream itself
    
* Left mouse selects actual packet
* Middle mouse zoom, shift zooms out
* s toggle rel/abs seq nrs
* t x axis show/on|f
* save to file: $import filename.extension & click op open window

### 9. Ping
Basic, but still very useful diagnostic.

    $ping -c x <trg>

Send x ICMP Echo request to trg

    #ping -f <trg>
    
Flood trg with ICMP echo request (user is limited to 200ms interval). DON'T do this if you don't own every machine on the path (and even then).

### 10. Ethtool
Directly control interface driver. Usually don't need it, but can save the day when you do. (actually that describes pretty much every command in a Linux console).

    $ethtool -k <iface>

Find out all parameters for the driver controlling iface. 

Example : you're benchmarking tcp but note that your wireshark capture shows tcp packets with segment sizes > MTU. You're seeing TCP offloading in action. To disable it, issue:

    #ethtool -K <iface> tso off
    
Disables tcp offloading (where the OS passes segments len>mtu to the NIC driver which then splits the segments into < mtu sized packets.

### 11. nuttcp
Transport layer benchmark/debug tool. Deprecates nttcp.

    #nuttcp -S
    
Establish a server on control tcp port 5000 responding to client requests.
alternative -6 for ipv6, -1 for single use only

    $nuttcp -l1200 -T30 -u -w4m -R1000m -N100 -i1 192.168.122.153

Test (interactive at 1s update) with a rate limit of 1G, 100 parallel portss, windows size 4m, segment length (targeted, may differ) of 1200 bytes using UDP, at a timeout of 30s.

    $nuttcp -vv -N100 -i1 192.168.122.153
    
Extra verbose 100 port parallel tcp test.

    $nuttcp -w1m 127.0.0.1 // or ::1

Test network stack (and interaction with CPU/MEM/IO) (providing localhost is responding) as a baseline. 

Args:

* -n x : send x packets only
* -w : window size, refers to buffer which has to match at receiver and sender, and only indirectly influences tcp window size (can be between w, 2*w on Linux.
* -N : parallel streams (each requiring an incremented port)
* -l b : payload size of b bytes (keep it under mtu!)
* -u : UDP


### 12. IPTables
Chains : INPUT/FORWARD/OUTPUT are XOR, only one is ever triggered, but depending on PRE/POST routing this can vary wildly:
IPTables control flow (highly reduced):

    packet = prerouting(packet);
    routingdecision = routing(packet);  
    if(routingdecision == destination_local){
        input_chain(packet);
        return;
    }
    else{
        forward_chain(packet);
    }
    postrouting(packet);
    
The output chain is hit for locally generated packets, it too passes postrouting in nat/mangle.
    
IPTables has tables and chains, each chain has n priority ordered rules on a first match basis, and a default policy.

    #iptables -P <chain> [DROP/ACCEPT/REJECT]

Assuming a machine with 2 ifaces, eth0 at LAN, eth1 at ISP/Internet:

    #iptables -t nat -A POSTROUTING -o outface -j MASQUERADE
    
Basic kitchen sink NAT. (sNAT, since source field is changed). In this case, packets destined for internet hit routing decision, FORWARD is hit, and then snat applied.

Since this is a rule running on a router, you'll need to configure the FORWARD chain as well:

    #iptables -A FORWARD -i eth1 -o eth0 -m state -–state RELATED,ESTABLISHED -j ACCEPT
    
This allows forwarding of internet to LAN traffic with state REL/EST. (across ifaces)

    #iptables -A FORWARD -i eth0 -o eth1 -j ACCEPT

LAN to Internet traffic is allowed to be forwarded.

To then have dNAT active :

    #iptables -t nat -A PREROUTING -i eth1 -p tcp --dport xy -j DNAT --tp xyzw:prt
    
So incoming packets for a dNAT'ted server from the internet, with TCP dport 80 and this
machina as target (NAT), have DNAT applied with xyzw:prt as address. (let xyzw:prt be your internal Apache server for example). This will still not work, since you'll need to adjust the FORWARD rule to allow NEW connections as well (inet to lan):

    #iptables -A FORWARD -i eth0 -p tcp --dport prt -d xyzw -j ACCEPT

    #iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 22 -j DNAT --to-destination xyzw:22
    
DMZ'ed server on port 22, with xyzw:22 outside of LAN.

Connection tracking : states NEW, INVALID, ESTABLISHED, RELATED
Psuedo-state for UDP, as in the traffic using same dst/src and ports is observed. ( a two way udp stream a:c<>b:d would trigger established. 
RELATED : Connection 1 relates to Connection 2, for example ftp control and ftp data are
clearly related.


### IPv6

#### {En/Dis}Able

If you configure iptables, check if you have ipv6 networking active and if so, you will need an ip6tables configuration as well.
Occassionaly you may want to disable ipv6 (suppose your ISP's router has no support for it, or for one reason or another the ipv6 stack gives poor performance ict ipv4). This isn't that likely anymore, but still.

Ipv6 is controlled by sysctl  
    $ sysctl net.ipv6.conf.<x>.<y>  

Where x is an interface name, or default/all, and y the actual setting. The same rules as in sysctl above apply. If you really want to disable the entire stack, add the kernel command line parameter "ipv6.disable=1". This is not recommended, more and more programs expect at least a passive ipv6 stack to be present. For Networkmanager, configuration can be set in gui.

#### Address configuration

IPv6 (6) has 128bit address, consisting of 8 ':' separated 16bit blocks, with each block in hex. Leading zeroes in a block can be omitted, consecutive zero blocks can be compressed using ::, following zero blocks can be noted as :0:.

The first 64 bits are the network prefix, the second half is the interface id. 
Interface ID is {Random, DHCP, Manual, EUI-64}.

EUI-64 :
    a':b:c:FF:FE:d:e:f  
Where abcdef is the MAC of the iface, and a' = a with bit [1] flipped. (right to left). 

The prefix is (usually) split into 48 bits for routing, and 16 for subnets (still half of v4 space). 

Private address (unique local address) range is fc00::/7 (128-7:121 bit range), prefix of 7 bit is reserved. Per RFC, use fd00::/8 since fc00::/7 is reserved.

In practice (RFC4193) : 7+1 bit for prefix, 40 random bit prefix identifier, 16bit subnet identifier, 64bit machine identifier. Basically half of ipv4 can be allocated to local domain. This then matches the global structure of an ipv6 address : [48bit netaddr][16bit subnetaddr][64bit devaddr]

Loopback : ::1

Zero:  ::



