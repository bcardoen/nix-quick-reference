Shell quick reference.
======================

### 0. Quick tips

#### 0.0 Basics

    $ man [n] cmd

Get the manpage for command, in section n if you need a specific page. A mistake I often make is first trying google before even looking at man, and while some pages can be quite overwhelming the information presented is almost always concise and correct.

    $ clear && clear

Useful to clearly mark a new console screen, especially after trying to scroll through repeated 1k+ error output of, for instance, a C++ compiler.

    $ source <file>

And more specific

    $ source ~/.bashrc  

    $ . <filename>  

Is called the source/dot operator. Instead of spawning a new shell in which filename is evaluated, the current shell is used. In source ~/bashrc this achieves the effect of altering the environment variables of the current shell, without having to relogin. 

#### 0.1 Redirection

    <commanda> | <commandb>  
    
Pipes output of a to input of b. 
E.g.  
     cat file.txt | grep mypassword  

will print each line from file.txt that contains the string mypassword.
    
    <commanda> > file  

Sends output from commanda to file, destroying any previous contents of that file and creating it if it did not exist before. Use >> to append, < to read from a file.

If you want to execute a command, and ignore any output and error  

    <cmd> > /dev/null 2>&1  

#### 0.2 Aliasing
   
      $alias  
Show you what alias'es are currently defined.

    $ alias youralias="mycommand"  
This is not persistent, so add it to ~/.bashrc to keep.

Example : I like to spawn new consoles from existing (instead of tabs), but the spawned terminal should remain even after the parent console exits. Since I have the annoying habbit of typing "also" in chat, let's combine all that   

    alias also='nohup /usr/bin/xfce4-terminal >/dev/null 2>&1 &'  

Nohup launches the process it receives as argument, but lets it ignore the signal SIGHUP (hence the name). So if the shell that executed the command exits and sends SIGHUP, the command keeps running. In addition to that, we avoid the creation of an output file by redirecting all output (and stderr) to the void. Finally, add an ampersand to execute it in the background (so we don't block the existing shell).  

Command substitution  

    $cat /boot/config-"$(uname -r)"  

Prints the current kernel (uname -r expands to the version nr) configuration. Can be achieved with backticks, but this makes nesting a nightmare. Quoting ensures the resulting output is not evaluated (think whitespace, globbing).


### 1. Tar
Create  
    $ tar -c<opts> <archive> [input]  

Typical onliner, create archive in current directory of all contents.   

    $ tar -czvf assignment.tgz .

-z : gzip

-v : verbose

-f : file (instead of redirection)


Extract (trgdir must exist)

    $ tar -xvzf <archive> -C <trgdir>


In modern versions, tar xf is sufficient (it detect the compression).  

### 2. Shell scripting
#### 2.0 Variables
    declare -[apUil] <varname>
* a var is an array
* p print variable & type
* U (zsh) : unique, ("a" "a") reduces to ("a") (remove by +U)
* i : integer

For example, creating an array:  

    $declare -a arr=("a" "b"  
    "c")  
Note that whitespace delimits array elements. 
Get the size of the array  

    $echo ${#array[@]}  

The hash operator returns the length of a variable.  
Accessing the array elements:

    $echo ${array[<index>]}  

#### 2.1 Flow control

    $for j in {2..5} 
    do ls -lashZ ./mydirv$j
    done

* Leave range out results in loop variable iterating over command line args, i.e. is equiv to 

    $for i in "$@"
    
* break|continue [x] : as in C, but with a nesting index, where x==1 is innermost enclosing loop body.  

A little shell script to show a couple of nix tools working together:

     #!/bin/bash
     IFACE=wlan0  
     TIMEOUT=2s
     # Get list of channels supported by interface, but remove the summary and only keep the actual channel nrs.
     iwlist $IFACE channel | awk '{if(NR>1) print $2;}' > channels.txt

     #You can do this at once with awk, but this is how this script grew.
     while read p; do    # Read channels
       echo "Testing channel $p"
       if expr $p + 0 > /dev/null 2>&1   # If $p is an integer
       then
         #Listen to channel $p for $TIMEOUT seconds, save and repeat.
         ifconfig $IFACE down
         iw dev $IFACE set type monitor
         ifconfig $IFACE up
         iw dev $IFACE set channel $p
         timeout -sHUP $TIMEOUT tcpdump -i $IFACE -w L2-3-1.$p.pcap   #Only capture for 2s per channel
       else
         echo "Skipping $p, not an integer."
       fi
    done <channels.txt



### 3. Hunting programs

    $ ps aux | grep <regex> 

Note that the "aux" are BSD style parameters, and perhaps not supported.  

The Grepped ps output gives you your target for the inevitable

    $ kill -<signal> <pid>

A typical example:  

    $kill x  

where x is the pid of the hostadp daemon, will shut it down and let it clean up before it exits.

Programs can register signal handlers, and you do have to be careful with which signal you send.
In order of niceness  

* TERM (default, allow cleanup by program)  

* ABRT stop immediately (can involve core dump), in C effect of calling abort()  

* KILL can't be ignored, halts immediately. (9)

* SEGV Emulate a segmentation fault (11). Useful for those rare occassions where you want to test this behaviour. In very rare circumstances (detached threads) can be more 'effective' than kill.

There are far more signals (eg stop/continue), see 

    $man 7 signal  

For runaway programs -9 does the trick nicely. Or -11 if you're harsh. Some pids are exempt to responding to kill (systemd), and it goes without saying that the ACL rules apply, you're free to kill your own programs but require sudo/su to go further. Warning: triple check that pid. Pid's are reused, and there's no end of fun if you   

    #kill -9 1111  

Only to find out that you wanted to kill 1112, and just murdered httpd on a production server. Kill has non-destructive uses as well:

    $ kill -USR1 <pid> 

Sends user defined signal 1 to pid. This is especially useful for dd, which prints a status report if it receives this signal. SIGHUP is mentioned in 0, for quite a few daemons this signal means : reload your configuration file (which you just edited), but as always check the man pages before you send signals.

    $ htop -d 2  

There's little you can't do with htop. -d <x> : update at 0.x s periods. It gives you a detailed view of all processes on the system, and allows you in ncurses to send signals as well (sidestepping typos).

### 4. Detective work
For networking state, see network qref.  

    $dmesg -H

Get the kernel log in (date) human readable format. Useful to see if that USB device you plugged in registered with the bus, or the firmware for that DVB you have on PCI-E got loaded or not.

    $lsusb [-vvv]

View all USB devices with increasing avalanches of information.

    $journalctl -xe

View systemlogs, starting at the [e]nd (where the interesting stuff tends to happen), and try to print an e[x]planation of the messages.

    $journalctl -xe --since today

since, until take time fields of the form "2012-10-30 18:17:16", but today/yesterday/tomorrow are understood. Also --until.

* --system, --user (or both if neither)  

* -k for dmesg.  

* --boot=[id][+-offset] , and ids are gotten via --list-boots  

### 5. Putting it all together.  

Let's say we have a drive (/dev/sdb) which needs zeroing before redeployment. A perfect tool for this is dd, but dd does not give any hint about the progress is making, unless you send it the USR1 signal. To do that, we need the pid, and to get that we need ps, grep and awk.  

    sudo kill -USR1 $(ps aux | grep "sudo dd if=" | awk 'NR==1 {print $2;}')  

First we ask ps for any processes running, grep the prefix of our command "sudo dd if=", feed that through awk and select the first line, 2nd field (pid). There will be multiple matches, since the grep itself is a process matching that expression. Then the PID is fed into kill, which then sends USR1 to dd.

### 6. Strings.

String manipulation in shell scripts

    string=ABC.dot
    sliced=${string%.dot}
    
Slices extension from filename

Prefix all matched strings:
In an editor (e.g. Sublime)
    pattern=[A-Za-z0-9]+\.dot
    replacepattern = prefix$0

### 7. Tmux

echo "set-option -g default-shell /bin/zsh" > ~/.tmux.conf

CTRL-B % Splits vertical pane

CTRL-B " Splits horizontal pane

CTRL-B Arrow moves to window

CTRL-B z full screen current

CTRL-B d detach current session


```
$tmux attach reattach
```

### 8. Tmux

You're logged in via ssh into another machine, and need to use the private/public key of your account on that machine. Plain ssh-add will not always work. 

    $eval `ssh-agent -s`

Followed by 

    $ssh-add
    
Will cache your passphrase for the keys (on the remote machine), this also means you don't need access to your local keys.
